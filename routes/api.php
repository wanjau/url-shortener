<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/testing-the-api', function() { 
    //return json
    //http://127.0.0.1:8000/api/testing-the-api

    return ['message' => 'hello'];
});

Route::namespace('App\Http\Controllers')->prefix('v1')->group(function () {
    Route::apiResource('links', 'LinkController');
});   

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
