<?php

namespace App\Http\Controllers;

use App\Models\Links;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class LinksController extends Controller
{
    public function index(){
        return view('shorten.index');
    }

    public function shorten(Request $request){

        $validateData = $request->validate([
            'original_link' => 'required|url'
        ]);

        $randomtoken = Str::random(8);

        $original_link = $request['original_link'];

        $links = new Links();

        $links->original_link = $original_link;
        $links->short_link = URL::to('/') . '/' .$randomtoken;

        //return $links;

        $links->save();

        return 'Shortened URL is' .' '. $links->short_link;
        
    }

    public function retrieveLink($link){

        $short_link = URL::to('/') . '/' .$link;

        $link = Links::where('short_link', $short_link)->first();
    
        if($link){
            return redirect($link->original_link);
        }else{
            return redirect('/');
        }
    }
}
