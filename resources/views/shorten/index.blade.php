<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>URl Shortener</title>

  <link rel="stylesheet" href="{{ asset('css/app.css') }}">

  <style>
    body{
      background-color: white;
    }

    .top-bar{
      background-color: purple;
      color: #fff;
    }
  </style>

</head>
<body>

<section class="top-bar py-5">
    <div class="top-bar text-center">
      <h2>Url Shortener</h2>
    </div>
</section>


<section class="py-5">

  <div class="container">
    <div class="row">
      <div class="col-md-10 mx-auto">

        <!-- <div class="panel panel-primary">
          <div class="panel-heading"></div>

          <div class="panel-body">
            <form action="" method="POST">
              @csrf

              <div class="form-group">
                <label for="shorten">Url to shorten</label>
                <input type="text" class="form-control" name="original_link" required placeholder="Enter url to shorten">

                 
              </div>

              <div class="form-group">
                <button type="submit" class="btn btn-primary">Shorten URL</button>  
              </div>
            
            </form>
          </div>
        </div> -->

        <h2>Below have highlighted the various api endpoints you can use when interacting with the api.</h2>

        <h3><u>For shortening a url</u></h3>

        <p><strong>Endpoint:</strong> http://127.0.0.1:8000/api/v1/links</p>
        <p><strong>Method:</strong> POST</p>
        <p>Parameter to pass:</p>
        <p><strong>Key:</strong> original_link <strong>value:</strong> https://www.infama.com/offers</p>

        <h3><u>Endpoint to view all the shortened urls</u></h3>
        <p><strong>Endpoint:</strong> http://127.0.0.1:8000/api/v1/links</p>
        <p><strong>Method:</strong> GET</p>


      </div>
    </div>
  </div> 

</section>

  <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>